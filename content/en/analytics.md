---
title: Analytics 📈
description: Grey Software's Open Analytics
category: Info
position: 6
---

Statistics provided by ![Plausible Analytics](/tech-stack/plausible-logo.png)

## Landing Website

### Monthly Analytics

<iframe plausible-embed src="https://plausible.io/share/grey.software?auth=k8ybZUWqj4n7wvl26NhNI&embed=true&theme=light" scrolling="yes" frameborder="0" loading="lazy" style="width: 1px; min-width: 100%; height: 550px;"></iframe>


**[Full Analytics](https://plausible.io/grey.software)**

## Org Website

### Monthly Analytics

<iframe plausible-embed src="https://plausible.io/share/org.grey.software?auth=cQh8gVI_y8KOiczjrwmfw&embed=true&theme=light" scrolling="yes" frameborder="0" loading="lazy" style="width: 1px; min-width: 100%; height: 550px;"></iframe>

**[Full Analytics](https://plausible.io/org.grey.software)**

## Ecosystem

### Monthly Analytics

<iframe plausible-embed src="https://plausible.io/share/ecosystem.grey.software?auth=YCYvxfWNbsfRvJukantOc&embed=true&theme=light" scrolling="yes" frameborder="0" loading="lazy" style="width: 1px; min-width: 100%; height: 550px;"></iframe>

**[Full Analytics](https://plausible.io/ecosystem.grey.software)**

## Learn

### Monthly Analytics

<iframe plausible-embed src="https://plausible.io/share/learn.grey.software?auth=zgkaBtGCGYoRfQ_j_ZyRR&embed=true&theme=light" scrolling="yes" frameborder="0" loading="lazy" style="width: 1px; min-width: 100%; height: 550px;"></iframe>

**[Full Analytics](https://plausible.io/learn.grey.software)**

## Glossary

### Monthly Analytics

<iframe plausible-embed src="https://plausible.io/share/glossary.grey.software?auth=6TBqBDbrKAcJe7xqwiI0Y&embed=true&theme=light" scrolling="yes" frameborder="0" loading="lazy" style="width: 1px; min-width: 100%; height: 550px;"></iframe>

[Full Analytics](https://plausible.io/glossary.grey.software)

## Links

### Monthly Analytics

<iframe plausible-embed src="https://plausible.io/share/links.grey.software?auth=R5AbQUp5qPgbMYvanJrto&embed=true&theme=light" scrolling="yes" frameborder="0" loading="lazy" style="width: 1px; min-width: 100%; height: 550px;"></iframe>

[Full Analytics](https://plausible.io/links.grey.software)

## Material Math

### Monthly Analytics

<iframe plausible-embed src="https://plausible.io/share/material-math.grey.software?auth=R4BQcZ1FJXAQmpkuSobSX&embed=true&theme=light" scrolling="yes" frameborder="0" loading="lazy" style="width: 1px; min-width: 100%; height: 550px;"></iframe>

[Full Analytics](https://plausible.io/material-math.grey.software)